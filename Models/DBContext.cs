﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace DashboardSvc.Models
{
    public partial class DBContext : DbContext
    {
        public DBContext(DbContextOptions<DBContext> options)
            : base(options)
        {
        }

        public virtual DbSet<TbConfig> TbConfigs { get; set; }
        public virtual DbSet<TbResult> TbResults { get; set; }

//        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
//        {
//            if (!optionsBuilder.IsConfigured)
//            {
//#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
//                //optionsBuilder.UseSqlServer("Server=DESKTOP-51NH7NC\\SQLEXP2014SP3;Database=InSituDB;user id=InSitu;password=insituanomaly");
//                optionsBuilder.UseSqlServer("Data Source = tcp:192.168.137.1, 1433; Initial Catalog = InSituDB; User ID = InSitu; Password = insituanomaly") ;  
//            }
//        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("Relational:Collation", "Latin1_General_CI_AI");

            modelBuilder.Entity<TbConfig>(entity =>
            {
                entity.HasKey(e => e.ConfigId)
                    .HasName("PK_tb_Config_ConfigID");

                entity.ToTable("tb_Config");

                entity.Property(e => e.ConfigId).HasColumnName("ConfigID");

                entity.Property(e => e.ConfigName).HasMaxLength(50);

                entity.Property(e => e.ConfigValue).HasMaxLength(50);

                entity.Property(e => e.CreatedBy).HasMaxLength(20);

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.ModifiedBy).HasMaxLength(20);

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.Remark).HasMaxLength(200);

                entity.Property(e => e.StatusInd)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<TbResult>(entity =>
            {
                entity.HasKey(e => e.ResultId)
                    .HasName("PK_tb_Result_ResultID");

                entity.ToTable("tb_Result");

                entity.Property(e => e.ResultId).HasColumnName("ResultID");

                entity.Property(e => e.WOId)
                    .IsRequired()
                    .HasMaxLength(10)
                    .HasColumnName("BatchID");

                entity.Property(e => e.CreatedBy).HasMaxLength(20);

                entity.Property(e => e.CreatedDateTime).HasColumnType("datetime");

                entity.Property(e => e.MachineId)
                    .HasMaxLength(10)
                    .HasColumnName("MachineID");

                entity.Property(e => e.ModifiedBy).HasMaxLength(20);

                entity.Property(e => e.ModifiedDateTime).HasColumnType("datetime");

                entity.Property(e => e.ProcessId)
                    .HasMaxLength(10)
                    .HasColumnName("ProcessID");

                entity.Property(e => e.Result).HasMaxLength(10);

                entity.Property(e => e.StatusInd)
                    .HasMaxLength(1)
                    .IsUnicode(false)
                    .HasDefaultValueSql("('Y')")
                    .IsFixedLength(true);

                entity.Property(e => e.WorkpieceId)
                    .HasMaxLength(10)
                    .HasColumnName("WorkpieceID");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
