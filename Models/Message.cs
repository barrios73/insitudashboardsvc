﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardSvc.Models
{
    public class Message<T>
    {
        public bool HasError { get; set; }
        public string ErrMsg { get; set; }
        public T Data { get; set; }
    }
}
