﻿using System;
using System.Collections.Generic;

#nullable disable

namespace DashboardSvc.Models
{
    public partial class TbResult
    {
        public long ResultId { get; set; }
        public string MachineId { get; set; }
        public string ProcessId { get; set; }
        public string WOId { get; set; }
        public string WorkpieceId { get; set; }
        public string Result { get; set; }
        public string StatusInd { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDateTime { get; set; }
    }
}
