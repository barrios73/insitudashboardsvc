﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace DashboardSvc
{
    public class RabbitMQWorkerService : BackgroundService
    {
        private readonly ILogger<RabbitMQWorkerService> _logger;
        private readonly IHubContext<NotifyHub> _hubContext;
        private ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;

        public RabbitMQWorkerService(ILogger<RabbitMQWorkerService> logger, IHubContext<NotifyHub> hubContext)
        {
            _logger = logger;
            _hubContext = hubContext;
        }

        public override Task StartAsync(CancellationToken cancellationToken)
        {
            string rabbitmqUri = Environment.GetEnvironmentVariable("RABBITMQ_URI");
            _connectionFactory = new ConnectionFactory
            {
                Uri = new Uri(rabbitmqUri)                
            };

            _connectionFactory.DispatchConsumersAsync = true;

            _connection = _connectionFactory.CreateConnection();

            _logger.LogInformation("RabbitMQ connection is opened.");

            _channel = _connection.CreateModel();

            _channel.ExchangeDeclare(exchange: "dashboard",
                        type: ExchangeType.Direct,
                        durable: true,
                        autoDelete: false);


            _channel.QueueDeclare(queue: "dashboard",
                durable: true, //was true
                exclusive: false,
                autoDelete: false,
                arguments: null);

            _channel.QueueBind(queue: "dashboard",
                              exchange: "dashboard",
                              routingKey: "dashboard");

            _channel.BasicQos(prefetchSize: 0, prefetchCount: 1, global: false);

            //_channel.QueueDeclarePassive(QueueName);
            //_channel.BasicQos(0, 1, false);
            //_logger.LogInformation($"Queue [{QueueName}] is waiting for messages.");

            return base.StartAsync(cancellationToken);
        }


        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var consumer = new AsyncEventingBasicConsumer(_channel);
            consumer.Received += async (sender, e) =>
            {

                var body = e.Body.ToArray();

                var message = Encoding.UTF8.GetString(body);
                
                _logger.LogInformation("Message {Id} consumed: ", e.DeliveryTag);
                //Debug.WriteLine("Send to debug output." + message);
                
                PredictionResult result = JsonSerializer.Deserialize<PredictionResult>(message);

                CheckIfResultOOS(result);

                //var connection = new HubConnectionBuilder()
                //    .WithUrl("http://192.168.137.1:5003/notify")    //192.168.137.1:5003/notify  172.20.115.26/DashboardSvc/notify
                //    .Build();

                //connection.StartAsync().Wait();
                
                
                await _hubContext.Clients.All.SendAsync("BroadcastPredictionResult", result);
                //connection.StopAsync().Wait();
            };

            _channel.BasicConsume("dashboard", true, consumer);
            await Task.CompletedTask;
        }


        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            await base.StopAsync(cancellationToken);
            _connection.Close();
            _logger.LogInformation("RabbitMQ connection is closed.");
        }


        private void CheckIfResultOOS(PredictionResult result)
        {
            if (result.OutputType == "numerical")
            {
                double numResult;
                if (Double.TryParse(result.PredictResult, out numResult))
                {
                    if (numResult < result.MinValueOutputParam || numResult > result.MaxValueOutputParam)
                    {
                        result.IsOOS = true;
                    }
                    else
                    {
                        result.IsOOS = false;
                    }
                }
            }
            else if (result.OutputType == "categorical")
            {
                List<string> actualLabels = result.ActualLabel.Split(",").ToList<string>();
                List<string> isOOSLabels = result.IsOOSLabel.Split(",").ToList<string>();

                if (actualLabels.IndexOf(result.PredictResult) != -1)
                {
                    result.IsOOS = bool.Parse(isOOSLabels[actualLabels.IndexOf(result.PredictResult)]);
                }
                else
                {
                    result.IsOOS = false;
                }
            }
        }
    }
}