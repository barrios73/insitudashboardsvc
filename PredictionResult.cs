﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardSvc
{
    public class PredictionResult
    {
        public string CorrelationId { get; set; }
        public string Topic { get; set; }
        public string[] InputParamNames { get; set; }
        public List<string> InputParamDesc { get; set; }
        public double[][] Data { get; set; }
        public string ModuleId { get; set; }
        public string ProductionLineId { get; set; }
        public string ProductId { get; set; }
        public string MachineId { get; set; }
        public string ProcessId { get; set; }
        public string WOId { get; set; }
        public string WorkpieceId { get; set; }
        public string ModelName { get; set; }
        public string Library { get; set; }
        public string PredictResult { get; set; }
        public string PassRate { get; set; }
        public DateTime? CreatedDateTime { get; set; }
        public bool IsOOS { get; set; } = false;
        public string TotalQty { get; set; }
        public string[][] MinValue { get; set; }
        public string[][] MaxValue { get; set; }
        public string OutputParameter { get; set; }
        public double? MinValueOutputParam { get; set; }
        public double? MaxValueOutputParam { get; set; }
        public string ActualLabel { get; set; }
        public string DisplayLabel { get; set; }
        public string IsOOSLabel { get; set; }
        public string OutputType { get; set; }
    }
}
