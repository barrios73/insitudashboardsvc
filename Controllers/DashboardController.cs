﻿using DashboardSvc.Models;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardSvc.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class DashboardController : Controller
    {
        private readonly DBContext _dBContext;
        private readonly ILogger<DashboardController> _logger;

        public DashboardController(DBContext dBContext, ILogger<DashboardController> logger)
        {
            _dBContext = dBContext;
            _logger = logger;
        }


        [HttpGet("GetPredictionResult")]
        public IActionResult Get()
        {
            List<PredictionResult> listPrediction = new List<PredictionResult>();
            listPrediction = GetPredictionResult();

            ObjectResult result = new ObjectResult(listPrediction);
            result.ContentTypes.Add("application/json");
            result.StatusCode = 200;

            return result;
        }



        private List<PredictionResult> GetPredictionResult()
        {
            List<PredictionResult> listPrediction = new List<PredictionResult>();
            try
            {
                TbConfig config = new TbConfig();

                config = (from c in _dBContext.TbConfigs
                          where c.StatusInd == "Y" && c.ConfigName == "PredictionResultStartTime"
                          select c).FirstOrDefault();

                DateTime startDateTime = DateTime.Today.AddHours(int.Parse(config.ConfigValue));
                if (DateTime.Now.CompareTo(startDateTime) < 0)
                {
                    startDateTime = startDateTime.AddDays(-1);
                }

                listPrediction = (from r in _dBContext.TbResults
                                  where r.StatusInd == "Y" && r.CreatedDateTime >= startDateTime
                                  orderby r.MachineId, r.ProcessId
                                  select new PredictionResult()
                                  {
                                      MachineId = r.MachineId,
                                      ProcessId = r.ProcessId,
                                      WOId = r.WOId,
                                      WorkpieceId = r.WorkpieceId,
                                      PredictResult = r.Result,
                                      PassRate = "",
                                      CreatedDateTime = r.CreatedDateTime
                                  }).ToList();
            }
            catch (Exception Ex)
            {
                throw (new Exception(Ex.Message));
            }

            return listPrediction;
        }


        //Get request
        [HttpGet("GetHistoryPredictionResult")]
        public IActionResult GetHistoryPredictionResult(DateTime start, DateTime end)
        {
            List<PredictionResult> listPrediction = new List<PredictionResult>();

            try
            {
                listPrediction = (from r in _dBContext.TbResults
                                  where r.StatusInd == "Y" && r.CreatedDateTime >= start && r.CreatedDateTime <= end
                                  orderby r.MachineId, r.ProcessId
                                  select new PredictionResult()
                                  {
                                      MachineId = r.MachineId,
                                      ProcessId = r.ProcessId,
                                      WOId = r.WOId,
                                      WorkpieceId = r.WorkpieceId,
                                      PredictResult = r.Result,
                                      PassRate = "",
                                      CreatedDateTime = r.CreatedDateTime
                                  }).ToList();
            }
            catch (Exception Ex)
            {
                throw (new Exception(Ex.Message));
            }

            ObjectResult result = new ObjectResult(listPrediction);
            result.ContentTypes.Add("application/json");
            result.StatusCode = 200;

            return result;
        }

    }
}
