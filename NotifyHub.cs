﻿using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardSvc
{
    public class NotifyHub : Hub
    {
        private readonly ILogger<NotifyHub> _logger;

        public NotifyHub(ILogger<NotifyHub> logger)
        {
            _logger = logger;
        }

        public override Task OnConnectedAsync()
        {
            var feature = Context.Features.Get<IHttpConnectionFeature>();
            _logger.LogInformation("Client connected with IP {RemoteIpAddress}", feature.RemoteIpAddress);
            return base.OnConnectedAsync();
        }

        public static string m_exePath = string.Empty;
        //public async Task BroadcastPredictionResult(PredictionResult prediction)
        //{
        //    _logger.LogInformation("Going to push results");
        //    await Clients.All.SendAsync("BroadcastPredictionResult", prediction);
        //}

        public async Task SendMessage(string userName, string message)
        {
            await Clients.All.SendAsync("ReceiveMessage", userName, message);
        }

        public void ClientConnected()
        {
            _logger.LogInformation("I am connected");
        }
    }
}
