using DashboardSvc;
using DashboardSvc.Models;
using Microsoft.EntityFrameworkCore;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
builder.Services.AddDbContext<DBContext>(options =>
{
    //options.UseSqlServer("Data Source = tcp:192.168.137.1, 1433; Initial Catalog = InSituDB; User ID = InSitu; Password = insituanomaly");
    options.UseSqlServer(Environment.GetEnvironmentVariable("DB_URL"));

});

builder.Services.AddCors(o => o.AddPolicy("CorsPolicy", builder =>
{
    builder
    .AllowAnyMethod()
    .AllowAnyHeader()
    .AllowAnyOrigin()
    .AllowCredentials()
    .WithOrigins("http://localhost:53402", "http://localhost:4200", "http://localhost", "http://192.168.137.1");
}));

builder.Services.AddHostedService<RabbitMQWorkerService>();
builder.Services.AddSignalR();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("CorsPolicy");
app.UseRouting();
app.UseAuthorization();

app.UseEndpoints(endpoints =>
{
    endpoints.MapHub<NotifyHub>("/notify");
    endpoints.MapControllers();
});

app.Run();
