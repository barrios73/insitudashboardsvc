﻿using DashboardSvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DashboardSvc
{
    public interface ITypedHubClient
    {
        //Task BroadcastPredictionResult(List<PredictionResult> prediction);
        Task BroadcastPredictionResult(List<PredictionResult> prediction, string test);
    }
}
